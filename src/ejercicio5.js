class MyClass {
    constructor() {
        this.names_ = [];
    }
        set name(value) {
        this.names_.push(value);
    }
        get name() {
        return this.names_[this.names_.length - 1];
    }
}

const myClassInstance = new MyClass();
myClassInstance.name = 'Joe';
myClassInstance.name = 'Bob';

// porque obtiene en ultimo nombre del array
console.log(myClassInstance.name);      // Res. en consola:  Bob

// porque crea una nueva clase MyClass tipo array con los nombres
console.log(myClassInstance.names_);    // Res. en consola:  ['Joe', 'Bob']