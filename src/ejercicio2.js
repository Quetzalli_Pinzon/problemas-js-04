var obj = {
    a: "hello",
    b: "this is",
    c: "javascript!",
};

// Object.values() devuelve un nuevo array que contiene los valores para cada índice del array
var array = Object.values(obj);
console.log(array); // ["hello", "this is", "javascript!"]
