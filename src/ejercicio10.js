function time() {
    date = new Date(); // constructor date me permite trabajar con fechas y horas
    hour = date.getHours(); // obtengo hora actual
    minute = date.getMinutes(); // obtengo minuto actual
    second = date.getSeconds(); // obtengo segundo actual
    if (hour < 10) { 
       hour ="0" + hour;
       }
    if (minute < 10) { 
       minute ="0" + minute;
       }
    if (second < 10) { 
       second ="0" + second;
       }
    // mostrar el reloj con este formato 
    clock = hour + " h : " + minute + " min : " + second +" seg";	
            return clock; 
    }

function updateClock() { 
showHour = time();
clock = document.getElementById("clock-id"); // busca en el html
clock.innerHTML = showHour; // incluyo en el html
}

setInterval(updateClock,1000); // setea un intervalo