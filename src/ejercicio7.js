class Queue {
    constructor () { 
        const list = []; 
        this.enqueue = function (type) { 
        list.push(type); 
        return type;
    };
        this.dequeue = function () { 
            return list.shift(); 
        };
    }
}
    
var q = new Queue; 

q.enqueue(9); // Es el primero en estrar
q.enqueue(8); //
q.enqueue(7); //
 
console.log(q.dequeue());       // 9 porque al ser el primero que esta es el array es el primero en salir
console.log(q.dequeue());       // 8 es el que queda al inicio y sale 
console.log(q.dequeue());       // 7 es el que queda al inicio y sale 
console.log(q);                 // Queue {enqueue: ƒ, dequeue: ƒ} porque ya no hay nada en el array 
console.log(Object.keys(q));    // ["enqueue","dequeue"] porque Object.keys devuelve un arrayde strings con las propiedades que se encuentran en el objeto