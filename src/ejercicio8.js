class Person{
    constructor(firstname, lastname){
        // propiedades no accesibles desde el exterior con __
        this.__firstname = firstname;
        this.__lastname = lastname;
        }
        get firstname() {
            return this.__firstname;
        }
        set firstname(name) {
            this.__firstname = name;
        }
        get lastname() {
            return this.__lastname;
        }
        set lastname(name) {
            this.__lastname = name;
        }

}

let person = new Person('John', 'Doe');
console.log(person.firstname, person.lastname); // En consola: John Doe
person.firstname = 'Foo';
person.lastname = 'Bar';
console.log(person.firstname, person.lastname); // En consola: Foo Bar