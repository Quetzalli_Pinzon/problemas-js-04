var anObject = {
    foo: 'bar',
    length: 'interesting',
    '0': 'zero!',
    '1': 'one!'
};

var anArray = ['zero.', 'one.']

// Imprime lo que hay en el array y objeto en la posición 0
console.log(anArray[0], anObject[0]);           // Resultados en consola: zero. zero! 

// Imprime lo que hay en el array y objeto en la posición 1
console.log(anArray[1], anObject[1]);           // Resultados en consola: one. one!

// Imprime la longitud del array y lo que hay en objeto en la propiedad length
console.log(anArray.length, anObject.length);   // Resultados en consola: 2 interesting

// Imprime undefined porque no hay nada que imprimir en el array con foo y bar porque imprime el valor de la propiedad foo que tiene el objeto
console.log(anArray.foo, anObject.foo);         // Resultados en consola: undefined bar


// imprime si el tipo de dato es igual a object pero no es igualdad estricta
console.log(typeof anArray == 'object', typeof anObject == 'object');   // Resultados en consola: true true

// Determina si pertenen a la clase Object y los dos son true
console.log(anArray instanceof Object, anObject instanceof Object);     // Resultados en consola: true true

// Determina si pertenen a la clase Array, anArray si y anObject no
console.log(anArray instanceof Array, anObject instanceof Array);       // Resultados en consola: true false

// imprime true porque anArray es un array y false porque anObject no es un array
console.log(Array.isArray(anArray), Array.isArray(anObject));           // Resultados en consola: true false