let zero = 0;
    function multiply(x) { return x * 2;}
    function add(a = 1 + zero, b = a, c = b + a, d = multiply(c)) {
    console.log((a + b + c), d);
}

// Cuál es el resultado de ejecutar las sig. sentencias?

add(1);             // Resultado en consola: 4, 4
add(3);             // Resultado en consola: 12, 12
add(2, 7);          // Resultado en consola: 18, 18
add(1, 2, 5);       // Resultado en consola: 8, 10
add(1, 2, 5, 10);   // Resultado en consola: 8, 10