const classInstance = new class {
    get prop() {
        return 5;
    }
};
    
classInstance.prop = 10;
console.log(classInstance.prop); // logs: 5

// porque si la classInstance obtiene prop va a retornar 5, siempre. 